let express = require('express')
let request = require('request')
let querystring = require('querystring')
const axios = require('axios')
let app = express()
var bodyParser = require('body-parser')
var fs = require('file-system')
var knex = require('knex')({
  client: 'mssql',
  connection:{
    server: '35.192.56.199',
    user: 'admin',
    password:'7895123Bb',
    database:'SpotifyTrackerDatabase',
    options:{
      port:1433,
      encrypt: true
    }
  },
  pool:{
    idleTimeoutMillis: 5000,
    destroyTimeoutMillis: 5000,
    propagateCreateError: false,
    createRetryIntervalMillis: 200,
  }
})

let redirect_uri =
  process.env.REDIRECT_URI ||
  'https://spotifytracker.boryssey.com/callback'
let client_id = process.env.SPOTIFY_CLIENT_ID || '4f64be38b7d441c78c8269795d8a385d';
let client_secret = process.env.SPOTIFY_CLIENT_SECRET || '4ac6e12329a247f1a882b84269673678';



app.use(bodyParser.json())

app.use(function(req, res, next) {
       res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
       res.header("Access-Control-Allow-Origin", "http://localhost:8080");
          next();
    });

app.get('/test', async function(req,res){
  try{
    const result = await findUserByName('boryssey')
    console.log(result)
  } catch(e){
    console.log(e)
  }
  res.send('testing')
})

app.get('/login', function(req, res) {
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: 'user-read-currently-playing',
      redirect_uri
    }))
})

app.get('/', function(req, res) {
  res.send('hello world');
})
app.get('/callback', async function(req, res) {

  if(req.query.error){
    res.redirect('http://localhost:8080/#/login?error=access_denied');
    console.log('ACCESS DENIED')
    return;
  }

  let code = req.query.code || null

  const url = 'https://accounts.spotify.com/api/token'
  const data = querystring.stringify({
    code: code,
    redirect_uri,
    grant_type: 'authorization_code',
  })
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret)).toString('base64'),
  }
  try {
    const tokens = await axios.post(url, data, {headers})
    const resp = await getUserInfo(tokens.data.access_token)

    let user = {
      id: resp.data.id,
      display_name: resp.data.display_name,
      access_token: tokens.data.access_token,
      refresh_token: tokens.data.refresh_token,
    }
    const result = await LoginOrRegister(user);

    res.redirect('http://localhost:8080/#/login?userid='+result.id +'&username='+result.display_name);
    console.log(result)
  } catch (e) {
    console.log(e)
  }

})

app.put('/me', async function(req, res){
  try{
    const userId = req.body.userId;
    const newDisplayName = req.body.display_name;

    if(userId === undefined || newDisplayName === undefined || userId === "" || newDisplayName === ""){
      res.status(404).send();
      return;
    }
    const user = await findUserById(userId);
    if(user === null || user === undefined){
      res.status(404).send();
      return;
    }
    else{
      console.log(user)
      try{
        var update = await updateUsernameById(userId, newDisplayName);
      }catch(err){
        console.log(err);
        res.status(500).send()
        return;
      }
      res.status(200).send();
      return;
    }
    console.log(req.body)

    res.json(req.body)
  }catch(error){
    console.log(error);
    res.status(500).send(error);
  }

})

app.get('/me/:id', async function(req,res){
  const userId = req.params.id;
  const user = await findUserById(userId);
  if(user != undefined){
    try{
      const result = await getUserInfo(user.access_token);

      console.log(result.data)
      res.status(200).send(constructUserInfo(result.data))

    }catch(e){
      console.log('error')
      console.log(e)
      if(e.response.status === 401){
        try{
          const refresh = await refreshToken(user.refresh_token)
          const updated = await updateAccessTokenById(user.id, refresh.data.access_token)
          const result = await getUserInfo(refresh.data.access_token);

          console.log(result.data)
          res.send(constructUserInfo(result.data))
        } catch(err){
          console.log(err)
          res.sendStatus(err.response.status)
        }
      } else{
        console.log(e);
      }

    }
  }
})

function constructUserInfo(data){
  console.log(data.images.length)
  let image = (data.images.length !== 0) ? data.images[0].url : "";
  console.log(image)
  let res = {
    id: data.id,
    display_name: data.display_name,
    profilePic: image
  }
  return res;
}


app.get('/users/:username', async function(req,res){
  const username = req.params.username;
  const user = await findUserByName(username)

  if(user === undefined){
    console.log('user not found')
    res.status(404).send();
    return
  }
  if(user !== null && user.display_name !== null && user.display_name !== 'null'){
    try{
      const result = await getCurrentPlaying(user.access_token);
      if(result.status === 204){
        console.log('Nothing is playing atm')
        res.sendStatus(204)
        return;
      }

      console.log(result.data)
      res.status(200).send(constructResponse(result.data, user))

    }catch(e){
      console.log('error')
      console.log(e)
      if(e.response.status === 401){
        try{
          const refresh = await refreshToken(user.refresh_token)
          const updated = await updateAccessTokenById(user.id, refresh.data.access_token)
          const result = await getCurrentPlaying(refresh.data.access_token);
          if(result.status == 204){
            console.log('Nothing is playing at the moment');
            res.sendStatus(204);
            return;
          }
          console.log(result.data)
          res.status(200).send(constructResponse(result.data, user))
        } catch(err){
          console.log(err)
          res.sendStatus(err.response.status)
        }
      } else{
        console.log(e);
      }

    }
  }
})

function constructResponse(data, user){
  if(data.item.is_local){
    const image = data.item.album.images.length !== 0 ? data.item.album.images[0].url : "";
    const artist = data.item.artists.map(artist => artist.name).join(', ');
    const songname = data.item.name;
    const link = "";
    const username = user.display_name;

    return {username, artist, songname, image, songname, link};
  }
  var username = user.display_name;
  var artist = data.item.artists.map(artist => artist.name).join(', ');
  var songname = data.item.name;
  const image = data.item.album.images.length !== 0 ? data.item.album.images[0].url : "";
  var link = data.item.external_urls.spotify;
  return {artist, songname, image, link, username};
}


function updateAccessTokenById(id, access_token){
  console.log('updating token for ' + id)
  return knex('Users').where('id', id).update('access_token', access_token).timeout(5000).then(data => data).catch(e => console.log(e))
}

function findUserById(id){
  return knex.select().from('Users').where('id', id).first().timeout(5000).then((data) => {return data}).catch(e => console.log(e))
}

function updateUsernameById(id, display_name){
  console.log('updating username for ' + id)
  return knex('Users').where('id', id).update('display_name', display_name).timeout(5000).then(data => data).catch(e => console.log(e))
}

function findUserByName(str){
  return knex.select().from('Users').where('display_name', str).first().timeout(5000).then((data) => {return data}).catch(e => console.log(e))
}

async function LoginOrRegister(user){
  const foundUser = await findUserById(user.id);
  const rg = new RegExp('^[A-Za-z0-9_.~-]+$');
  var result = {}
  if(foundUser === undefined){
    const foundUserByName = await findUserByName(user.display_name)
    if( foundUserByName !== undefined || user.display_name === 'null' || user.display_name === null || user.display_name === '' || !rg.test(user.display_name)){
      user.display_name = user.id
    }
    result = await knex('Users').returning(['id','display_name','access_token','refresh_token']).insert(user).timeout(5000).then(data => data).catch(e=> console.log('error while adding new user ' + e))
    result = result[0]
  } else{
    result = foundUser;
  }
  return result;
}



function refreshToken(refresh_token){
  let url = 'https://accounts.spotify.com/api/token';
  let headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret)).toString('base64'),
  }
  let data = querystring.stringify({
    refresh_token: refresh_token,
    grant_type: 'refresh_token',
  })
  console.log('refreshing token')
  return axios.post(url, data, {headers});
}

function getUserInfo(access_token) {
  let url = 'https://api.spotify.com/v1/me';
  let headers = {
    'Authorization': 'Bearer ' + access_token
  }
  return axios.get(url, {headers});
}


function getCurrentPlaying(access_token) {
  const url = 'https://api.spotify.com/v1/me/player/currently-playing';
  const headers = {
    'Authorization': 'Bearer ' + access_token
  }
  return axios.get(url, {headers});
}

let port = process.env.PORT || 80
console.log(`Listening on port ${port}. Go /login to initiate authentication flow.`)
app.listen(port)
